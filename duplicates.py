import spacy
def remove_duplicates(hybrid_keywords):
	unique_key_phrases = list(set(hybrid_keywords))
	my_dict = dict()
	for key_phrase in unique_key_phrases:
		nlp = spacy.load('en')
		key_phrase_doc = nlp(key_phrase)
		word_list = []
		for word in key_phrase_doc:
			word = word.lemma_
			word_list.append((word))
		stem_key_phrase = " ".join(word_list)
		for word in word_list:
			if word in my_dict.keys():
				if stem_key_phrase not in my_dict.values():
					my_dict[word].append((stem_key_phrase))
			else:
				my_dict[word] = [stem_key_phrase]
	key_phrase_count = {}
	for word, key_phrases in my_dict.items():
		for key_phrase in key_phrases:
			if key_phrase in key_phrase_count:
				key_phrase_count[key_phrase] += 1
			else:
				key_phrase_count[key_phrase] = 1
	print("Count of key phrases")
	print(key_phrase_count)
	key_phrase_sorted = sorted(key_phrase_count, key=key_phrase_count.get, reverse=True)
	sorted_key_phrase_list = []
	for s_key_key_phrase in key_phrase_sorted:
		sorted_key_phrase_list.append((s_key_key_phrase))
	return sorted_key_phrase_list