from key_word_generation import get_keywords
from handler import generate_keywords
import os
import json


# input_dir = '/home/user/Documents/transcripts/high_confidence_transcripts/sample/'
# keywords_dir = "/home/user/Documents/transcripts/high_confidence_transcripts/output_high_conf_transcripts_no_fillers_keywords/spacy_and_bert/"
# keywords_dir_out = "/home/user/Documents/transcripts/high_confidence_transcripts/output_high_conf_transcripts_no_fillers_keywords/keywords/"


def get_keywords_from_file(keywords_dir, keywords_dir_out):
    flag = 0
    for subdir, dirs, files in os.walk(keywords_dir):
        for file in files:
            print('Reading--->', file)
            with open(os.path.join(subdir, file), 'r') as fP:
                keywords_dict = json.load(fP)
            print(keywords_dict)
            keywords_list = {'keywords': list()}
            moments = keywords_dict['keywords']
            for moment_keyword in moments:
                flag += 1
                transcript_with_keywords = {'moment': moment_keyword['moment'], 'rake': list(), 'spacy': list(),
                                            'bert': list(), 'spacy_bert': list()}
                for moment_keyword_list in moment_keyword['keywords_list']:
                    for key, value in moment_keyword_list.items():
                        for k, v in value[0].items():
                            if v != 0.0:
                                transcript_with_keywords[k].append(key)
                if len(transcript_with_keywords['spacy']) + len(transcript_with_keywords['rake']) + len(
                        transcript_with_keywords['bert']) > 0:
                    if len(set(transcript_with_keywords['spacy']) - set(transcript_with_keywords['bert'])) != 0:
                        transcript_with_keywords['spacy_bert'] = list(
                            set(transcript_with_keywords['spacy']) - set(transcript_with_keywords['bert']))
                    keywords_list['keywords'].append(transcript_with_keywords)
            keywords_file = os.path.join(keywords_dir_out, file.split('.')[0] + '.json')
            with open(keywords_file, 'w') as fP:
                json.dump(keywords_list, fP, indent=4)
    print(flag)


# get_keywords_from_file(keywords_dir, keywords_dir_out)

input_dir = '/home/user/Documents/Semeval2010_Abstracts/'
# input_dir = '/home/user/Documents/sample/'
output_dir = '/home/user/Documents/Semeval_out/'


def get_keywords_from_abstracts(input_dir, output_dir):
    flag = 0
    rake_list = list()
    bert_list = list()
    spacy_list = list()

    rake_list_missed = list()
    bert_list_missed = list()
    spacy_list_missed = list()
    for subdir, dirs, files in os.walk(input_dir):
        for file in files:
            try:
                if '.key' in file:
                    print('Reading--->', file)
                    fP =  open(os.path.join(subdir, file), 'r')
                    keywords_lines = fP.readlines()
                    true_keywords = list()
                    for item in keywords_lines:
                        if item.strip():
                            true_keywords.append(item.strip())

                    fP = open(os.path.join(subdir, file.split(".")[0] + '.abstr'), 'r')
                    abstract_lines = fP.readlines()

                    abstract = list()
                    for line in abstract_lines:
                        if line.strip():
                            abstract.append(line.strip())
                    abstract = ' '.join(abstract)
                    abstract_with_keywords = {'abstract': abstract, 'rake': list(), 'spacy': list(), 'bert': list(),
                                              'true_keywords': true_keywords, 'accuracy': dict()}
                    keywords_list = generate_keywords(abstract)["keywords"]
                    for keyword_dict in keywords_list:
                        for key, value in keyword_dict.items():
                            for k, v in value[0].items():
                                if v != 0.0:
                                    abstract_with_keywords[k].append(key)
                    len_true = len(true_keywords)
                    rake_acc = len(
                        set(abstract_with_keywords['rake']) & set(abstract_with_keywords['true_keywords'])) / len_true
                    spacy_acc = len(
                        set(abstract_with_keywords['spacy']) & set(abstract_with_keywords['true_keywords'])) / len_true
                    bert_acc = len(
                        set(abstract_with_keywords['bert']) & set(abstract_with_keywords['true_keywords'])) / len_true

                    rake_missed = len(
                        set(abstract_with_keywords['rake']) - set(abstract_with_keywords['true_keywords'])) / len(set(abstract_with_keywords['rake']))
                    spacy_missed = len(
                        set(abstract_with_keywords['spacy']) - set(abstract_with_keywords['true_keywords'])) / len(set(abstract_with_keywords['spacy']))
                    bert_missed = len(
                        set(abstract_with_keywords['bert']) - set(abstract_with_keywords['true_keywords'])) / len(set(abstract_with_keywords['bert']))

                    abstract_with_keywords['accuracy'] = {'rake_acc': rake_acc, 'spacy_acc': spacy_acc, 'bert_acc': bert_acc, 'rake_missed': rake_missed, 'spacy_missed': spacy_missed, 'bert_missed': bert_missed}
                    rake_list.append(rake_acc)
                    spacy_list.append(spacy_acc)
                    bert_list.append(bert_acc)

                    rake_list_missed.append(rake_missed)
                    spacy_list_missed.append(spacy_missed)
                    bert_list_missed.append(bert_missed)
                    keywords_file = os.path.join(output_dir, file.split('.')[0] + '.json')
                    with open(keywords_file, 'w') as fP:
                        json.dump(abstract_with_keywords, fP, indent=4)
                    print(abstract_with_keywords)
            except Exception as e:
                print("=======!!!!!!!!!!!=========")
                print(file)
                print("=======!!!!!!!!!!!=========")
                pass
    #
    # print(rake_list)
    # print(spacy_list)
    # print(bert_list)
    #
    # print(rake_list_missed)
    # print(spacy_list_missed)
    # print(bert_list_missed)

    print('Rake acc', sum(rake_list)/ len(rake_list))
    print('Spacy acc', sum(spacy_list)/ len(spacy_list))
    print('Bert acc', sum(bert_list)/ len(bert_list))

    print('Rake missed', sum(rake_list_missed)/ len(rake_list_missed))
    print('Spacy missed', sum(spacy_list_missed)/ len(spacy_list_missed))
    print('Bert missed', sum(bert_list_missed)/ len(bert_list_missed))

get_keywords_from_abstracts(input_dir, output_dir)
