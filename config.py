import os


PORT = os.getenv('PORT', 8096)
HOST = os.getenv('HOST', '0.0.0.0')
DEBUG = os.getenv('DEBUG', True)
STOP_WORD_PATH = "data_pipleline/stopwords.txt"
