import nltk
import itertools
import string
import networkx
import huddl_logger_python as logger
from helper import load_stop_words, nlp
from itertools import takewhile, tee
import benepar
import spacy
from benepar.spacy_plugin import BeneparComponent
import time


module_tag = "pattern_textrank_extractor.py"


def load_bert_model(nlp):
    try:
        nlp.add_pipe(BeneparComponent("benepar_en2"))
    except Exception as e:
        benepar.download('benepar_en2')
        nlp.add_pipe(BeneparComponent("benepar_en2"))
    return nlp


nlp_bert = spacy.load('en_core_web_sm')
load_bert_model(nlp_bert)


def extract_candidate_chunks(text, grammar=r'KT: {(<JJ>* <NN.*>+ <IN>)? <JJ>* <NN.*>+}'):
    """
    TODO we are not using this currently
    :param text:
    :param grammar:
    :return:
    """
    # exclude candidates that are stop words or entirely punctuation
    try:
        punct = set(string.punctuation)
        stop_words = load_stop_words()
        # tokenize, POS-tag, and chunk using regular expressions
        chunker = nltk.chunk.regexp.RegexpParser(grammar)
        tagged_sents = []
        for sent in nltk.sent_tokenize(text):
            tagged_sents.append(nltk.pos_tag(nltk.word_tokenize(sent)))
        all_chunks = list(itertools.chain.from_iterable(nltk.chunk.tree2conlltags(chunker.parse(tagged_sent))
                                                        for tagged_sent in tagged_sents))

        # join constituent chunk words into a single chunked phrase

        candidates = [' '.join(word for word, pos, chunk in group).lower()
                      for key, group in itertools.groupby(all_chunks, lambda x: x[2] != 'O') if key]

        return [cand for cand in candidates
                if cand not in stop_words and not all(char in punct for char in cand)]
    except Exception as e:
        logger.error(module_tag, e)


def extract_candidate_noun_chunks(nlp, text):
    try:

        doc = nlp(text)
        # exclude candidates that are stop words or entirely punctuation
        punct = set(string.punctuation)
        time_1 = time.time()
        stop_words = load_stop_words()
        time_1 = time.time()

        candidates = list(itertools.chain.from_iterable(i.text.lower().split() for i in doc.noun_chunks))
        candidates = [cand for cand in candidates
                if cand not in stop_words and not all(char in punct for char in cand)]

        return candidates
    except Exception as e:
        logger.error(module_tag, e)


def extract_candidate_words(text, good_tags=set(['JJ', 'JJR', 'JJS', 'NN', 'NNP', 'NNS', 'NNPS'])):
    """
     TODO we are not using this currently
    :param text:
    :param good_tags:
    :return:
    """
    try:
        # exclude candidates that are stop words or entirely punctuation
        punct = set(string.punctuation)
        stop_words = load_stop_words()
        # tokenize and POS-tag words
        tagged_sents = []
        for sent in nltk.sent_tokenize(text):
            tagged_sents.append(nltk.pos_tag(nltk.word_tokenize(sent)))
        tagged_words = itertools.chain.from_iterable(tagged_sents)
        # filter on certain POS tags and lowercase all words
        candidates = [word.lower() for word, tag in tagged_words
                      if
                      tag in good_tags and word.lower() not in stop_words and not all(char in punct for char in word)]
        return candidates
    except Exception as e:
        logger.error(module_tag, e)


def score_keyphrases_by_textrank(text, n_keywords=1, min_length=2):
    nlp_1 = spacy.load('en_core_web_sm')
    '''
    :param text:
    :param n_keywords:  percentage of keywords to be taken
    :param min_length:
    :return:
    '''
    try:
        # tokenize for all words, and extract *candidate* words
        words = [word.lower()
                 for sent in nltk.sent_tokenize(text)
                 for word in nltk.word_tokenize(sent)]
        candidates = extract_candidate_noun_chunks(nlp_1, text)
        # candidates = extract_candidate_noun_chunks(text)

        # build graph, each node is a unique candidate
        graph = networkx.Graph()
        graph.add_nodes_from(set(candidates))
        # iterate over word-pairs, add unweighted edges into graph
        def pairwise(iterable):
            """s -> (s0,s1), (s1,s2), (s2, s3), ..."""
            a, b = tee(iterable)
            next(b, None)
            return zip(a, b)

        for w1, w2 in pairwise(candidates):
            if w2:
                graph.add_edge(*sorted([w1, w2]))

        # score nodes using default pagerank algorithm, sort by score, keep top n_keywords
        ranks = networkx.pagerank(graph)
        if 0 < n_keywords <= 1:
            n_keywords = int(round(len(candidates) * n_keywords))

        word_ranks = {word_rank[0]: word_rank[1]
                      for word_rank in sorted(ranks.items(), key=lambda x: x[1], reverse=True)[:n_keywords]}

        keywords = set(word_ranks.keys())
        # merge keywords into keyphrases
        keyphrases = {}
        j = 0
        for i, word in enumerate(words):
            if i < j:
                continue
            if word in keywords:
                kp_words = list(takewhile(lambda x: x in keywords, words[i:i + 10]))
                avg_pagerank = sum(word_ranks[w] for w in kp_words) / float(len(kp_words))
                keyphrases[' '.join(kp_words)] = avg_pagerank
                # counter as hackish way to ensure merged keyphrases are non-overlapping
                j = i + len(kp_words)

        filtered_dict = {k: v for k, v in keyphrases.items() if len(k.split()) > min_length - 1}
        return sorted(filtered_dict.items(), key=lambda x: x[1], reverse=True)

    except Exception as e:
        logger.error(module_tag, e)



def score_keyphrases_by_textrank_bert(text, n_keywords=1, min_length=2):
    '''
    :param text:
    :param n_keywords:  percentage of keywords to be taken
    :param min_length:
    :return:
    '''
    try:
        # tokenize for all words, and extract *candidate* words
        words = [word.lower()
                 for sent in nltk.sent_tokenize(text)
                 for word in nltk.word_tokenize(sent)]

        time_1 = time.time()
        candidates = extract_candidate_noun_chunks(nlp_bert, text)
        logger.info(module_tag, "Get Noun Chunks {0:.2f} seconds".format(time.time()-time_1))


        time_1 = time.time()

        # build graph, each node is a unique candidate
        graph = networkx.Graph()
        graph.add_nodes_from(set(candidates))
        # iterate over word-pairs, add unweighted edges into graph
        def pairwise(iterable):
            """s -> (s0,s1), (s1,s2), (s2, s3), ..."""
            a, b = tee(iterable)
            next(b, None)
            return zip(a, b)

        for w1, w2 in pairwise(candidates):
            if w2:
                graph.add_edge(*sorted([w1, w2]))

        # score nodes using default pagerank algorithm, sort by score, keep top n_keywords
        ranks = networkx.pagerank(graph)
        if 0 < n_keywords <= 1:
            n_keywords = int(round(len(candidates) * n_keywords))

        word_ranks = {word_rank[0]: word_rank[1]
                      for word_rank in sorted(ranks.items(), key=lambda x: x[1], reverse=True)[:n_keywords]}

        keywords = set(word_ranks.keys())

        # merge keywords into keyphrases
        keyphrases = {}
        j = 0
        for i, word in enumerate(words):
            if i < j:
                continue
            if word in keywords:
                kp_words = list(takewhile(lambda x: x in keywords, words[i:i + 10]))
                avg_pagerank = sum(word_ranks[w] for w in kp_words) / float(len(kp_words))
                keyphrases[' '.join(kp_words)] = avg_pagerank
                # counter as hackish way to ensure merged keyphrases are non-overlapping
                j = i + len(kp_words)
        logger.info(module_tag, "Page rank processing time {0:.2f} seconds".format(time.time()-time_1))

        filtered_dict = {k: v for k, v in keyphrases.items() if len(k.split()) > min_length - 1}
        return sorted(filtered_dict.items(), key=lambda x: x[1], reverse=True)

    except Exception as e:
        logger.error(module_tag, e)