import re
import huddl_logger_python as logger
from string import punctuation

module_tag = "preprocessing.py"


def preprocess_text(line):
    try:
        line = line.strip()
        line = re.sub(' +', ' ', line)
        line = re.sub(' +', ' ', line)
        line = re.sub('-', ' ', line)
        punc = dict.fromkeys(punctuation)
        punc.pop(".", None)  # use dot for sentence end
        translator = str.maketrans(punc)
        line = line.translate(translator)
        return line

    except Exception as e:
        logger.error(module_tag, e)
