import re
from rake_nltk import Rake
import pandas as pd
from src.pattern_textrank_extractor import score_keyphrases_by_textrank, score_keyphrases_by_textrank_bert
from sklearn.preprocessing import MinMaxScaler
import sys
from helper import load_stop_words
import string
import huddl_logger_python as logger

scaler = MinMaxScaler()

sys.path.append(".")
from src.preprocessing import preprocess_text

module_tag = "key_phrase_extractor.py"


def ensemble_keyword_extractors(text):
    try:
        text = preprocess_text(text)
        stop_word_list = load_stop_words()

        ##get rake keywords
        # r = Rake(min_length=2, max_length=4,
        #          stopwords=stop_word_list,
        #          punctuations=string.punctuation)
        # # Uses stopwords for english from NLTK, and all puntuation characters.
        #
        # r.extract_keywords_from_text(text.lower())
        # rake_df = pd.DataFrame(r.get_ranked_phrases_with_scores(), columns=['rake', 'keywords'])

        #get spacy keywords
        # textrank_df = pd.DataFrame(score_keyphrases_by_textrank(text), columns=["keywords", "textrank_spacy"])
        textrank_df_bert = pd.DataFrame(score_keyphrases_by_textrank_bert(text), columns=["keywords", "textrank_bert"])

        # filter common phrases from rake extraction
        # common_phrases_index = []
        # for i in textrank_df["keywords"]:
        #     for j in rake_df.iterrows():
        #         if re.search(i, j[1][1]):
        #             common_phrases_index.append(j[0])
        # rake_df = rake_df.drop(common_phrases_index)

        # rake_extracted = rake_df
        ##merge rake, bert and spacy
        # result_df = pd.merge(rake_df, textrank_df, on="keywords", how="outer")
        # result_df = pd.merge(result_df, textrank_df_bert, on="keywords", how="outer")
        # result_df = result_df.fillna(0)

        # scaling and sorting dataframes
        # if not result_df.empty:
        #     result_df[["textrank", "rake"]] = scaler.fit_transform(result_df[["textrank", "rake"]])
        #
        #     result_df["total_score"] = result_df.apply(lambda x: x["rake"] + x["textrank"], axis=1)
        #
        #     result_df = result_df.sort_values(by=["total_score"], ascending=False)

        result_df = textrank_df_bert
        return result_df
    except Exception as e:
        logger.error(module_tag, e)
