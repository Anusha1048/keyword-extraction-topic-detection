from flask import Flask, jsonify, request
import json
import huddl_logger_python as logger
from handler import generate_keywords
from duplicates import remove_duplicates
import huddl_prometheus as hp

BODY = "body"
INVALID_INPUT = "INVALID_INPUT"
ERROR = "error"
module_tag = "routes.py"
app = Flask(__name__)

hp.setup_metrics(app, 'topic_generator_service')

@app.route("/healthcheck", methods=['GET'])
def healthCheck():
    return jsonify({"health": "ok"}), 200


@app.route("/get_topic", methods=['POST'])
def get_topic():
    if request.is_json:
        req = request.get_json()
    else:
        logger.error(module_tag, "Invalid json")
        return jsonify({ERROR: INVALID_INPUT}), 400

    if BODY not in req:
        logger.error(module_tag, ("Missing fields %s", req))
        return jsonify({ERROR: INVALID_INPUT}), 400

    try:
        response = {
            "keywords":list()
        }
        hybrid_keywords = generate_keywords(req[BODY])
        # hybrid_keywords=remove_duplicates(hybrid_keywords)
        response["keywords"] = hybrid_keywords

    except Exception as e:
        logger.error(module_tag, e)
        return jsonify({ERROR: "INTERNAL_SERVER_ERROR"}), 500
    return jsonify(response), 200

