import config
import nltk
import spacy

nlp = spacy.load('en_core_web_sm')


def load_stop_words(stop_word_file=config.STOP_WORD_PATH):
    """
    Utility function to load stop words from a file and return as a list of words
    @param stop_word_file Path and file name of a file containing stop words.
    @return list A list of stop words.
    """
    stop_words = []
    for line in open(stop_word_file):
        if line.strip()[0:1] != "#":
            for word in line.split():  # in case more than one per line
                stop_words.append(word)

    stop_words = list(set(stop_words + nltk.corpus.stopwords.words('english')))
    return stop_words
