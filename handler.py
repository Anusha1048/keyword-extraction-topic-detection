from key_phrase_extractor import ensemble_keyword_extractors
import huddl_logger_python as logger

module_tag = "handler.py"


def generate_keywords(text):
    try:
        keywords_list = list()
        result_df = ensemble_keyword_extractors(text)

        for row in result_df.iterrows():
            keywords_list.append(row[1][0])
            # keywords_list.append({row[1][1]: [{'rake': row[1][0], 'spacy': row[1][2], 'bert': row[1][3]}]})
        return keywords_list[:5]
    except Exception as e:
        logger.error(module_tag, e)